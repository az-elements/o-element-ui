import React, {Component} from 'react';
import {
  Button,
  Col,
  Collapse,
  Container,
  Media,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  Row
} from 'reactstrap';
import Image from 'react-bootstrap/Image';
import mockData from './mockData.js';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      categories: mockData
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  createSection = (section, index) => {
    let rows = []

    for (let i = 0; i < 2; i++) {
      rows.push(this.createRow(section, i))
    }

    return (
      <Container key={`container${index}`} className="section my-4">
        <h1 className="section__headline">{section.UXElement.Title}</h1>
        {rows}
        <footer>
          <NavLink href="/" className="text-right">{section.UXElement.Footer} ></NavLink>
        </footer>
      </Container>
    )
  }

  createRow = (section, key) => {
    let numColumns = section.UXElement.Columns
    let columns = []

    for (let i = 0; i < numColumns; i++) {
      columns.push(this.createArticle(section, 12 / numColumns, key, `${key}-${i}`))
    }

    return (
      <Row key={key}>
        {columns}
      </Row>
    )
  }

  createArticle = (section, columnWidth, columnHeight, key) => {
    if (columnHeight === 1) {
      return (
        <Col xs={columnWidth} key={key} className="mb-4">
          <NavLink href={section.DataElement.DataSourceURL}>
            <Media>
              <Row>
                <Col xs={8}>
                  <Media-Body>
                    <div className="newsItem__logo my-2">Logo</div>
                    <h2 className="newsItem__title--small">Title or Excerpt...Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</h2>
                    <footer>
                      <div className="newsItem__time text-muted my-2">1h ago</div>
                    </footer>
                  </Media-Body>
                </Col>
                <Col xs={4}>
                  <Image fluid rounded src="holder.js/128x128" alt="Generic placeholder"/>
                </Col>
              </Row>
            </Media>
          </NavLink>
        </Col>
      )
    }

    return (
      <Col xs={columnWidth} key={key} className="mb-4">
        <NavLink href={section.DataElement.DataSourceURL}>
          <Media>
            <Media-Body>
              <Image fluid rounded src="holder.js/768x256" alt="Generic placeholder"/>
              <div className="newsItem__logo my-2">Logo</div>
              <h2 className="newsItem__title">Title or Excerpt...Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</h2>
              <footer>
                <div className="newsItem__time text-muted my-2">1h ago</div>
              </footer>
            </Media-Body>
          </Media>
        </NavLink>
      </Col>
    )
  }

  render() {
    return (<div>
      <Navbar color="inverse" light expand="md">
        <NavbarBrand href="/">News App Demo</NavbarBrand>
        <NavbarToggler onClick={this.toggle}/>
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <Button tag="a" color="success" size="large" href="http://reactstrap.github.io" target="_blank">View Reactstrap Docs</Button>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>

      {this.state.categories.Sections.map((section, index) => this.createSection(section, index))}
      <p>Generated with the following mock JSON data:</p>
      <code>
        {JSON.stringify(mockData, null, '\t')}
      </code>
    </div>);
  }
}

export default App;
