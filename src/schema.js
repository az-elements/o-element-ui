{
  “OElement”: {
    “ID”: String,
    “Name”: String
  },
  [{
      ”Category”: {
        ”Name”: “TopStories”,
        “Type”: String
      },
      “UXElement”: {
        “Title”: “Top Stories”,
        “CSSURL”: css / TopStories.css,
        “Columns”: 2,
        “Footer”: “More Top Stories”,
        “FooterURL”: URL
      },
      “DataElement”: {
        “DataSourceURL”: URL
      }
    },
    {
      ”Category”: {
        ”Name”: “Sports”,
        “Type”: String
      },
      “UXElement”: {
        “Title”: “Sports”,
        “CSSURL”: css / Sports.css,
        “Columns”: 3,
        “Footer”: “More on Sports”,
        “FooterURL”: URL
      },
      “DataElement”: {
        “DataSourceURL”: URL
      }
    },
  ]
}
