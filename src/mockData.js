export default {
  OElement: {
    ID: "ID String",
    Name: "Name String"
  },
  Sections: [{
      Category: {
        Name: "TopStories",
        Type: "Category Type String"
      },
      UXElement: {
        Title: "Top Stories",
        CSSURL: "css/TopStories.css",
        Columns: 1,
        Footer: "More Top Stories",
        FooterURL: "FooterURL"
      },
      DataElement: {
        DataSourceURL: "DataSourceURL"
      }
    },
    {
      Category: {
        Name: "Sports",
        Type: "Category Type String"
      },
      UXElement: {
        Title: "Sports",
        CSSURL: "css / Sports.css",
        Columns: 4,
        Footer: "More on Sports",
        FooterURL: "FooterURL"
      },
      DataElement: {
        DataSourceURL: "DataSourceURL"
      }
    },
  ]
};
