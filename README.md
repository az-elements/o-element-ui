This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

## SOFTWARE COPYRIGHT AGREEMENT

THIS SOFTWARE IS PROTECTED BY COPYRIGHT LAWS AND INTERNATIONAL COPYRIGHT TREATIES, AS WELL AS OTHER INTELLECTUAL PROPERTY LAWS AND TREATIES.

### Copyright © 2023, OpenView Tech Inc. All rights reserved worldwide.

All rights, titles, interests, and all applicable copyrights, trade secrets, patents, trademarks, and any other intellectual property rights in and to the Software are owned by Openview Tech Inc. Unauthorized reproduction, distribution, transmission, display, publication, sale, licensing, or any other misuse of the Software or any portion of it is strictly prohibited. Any such unauthorized use shall result in immediate and automatic termination of this license and may result in criminal and/or civil prosecution.

No portion of this Software may be copied, reproduced, modified, translated, adapted, reverse engineered, or utilized in any manner on any platform or for any purpose without the express prior written consent of Openview Tech Inc.

ANY VIOLATION OF THIS AGREEMENT WILL BE VIGOROUSLY ENFORCED AND MAY SUBJECT THE OFFENDER TO CIVIL AND CRIMINAL PENALTIES.

BY ACCESSING, DOWNLOADING, COPYING, OR OTHERWISE USING THE SOFTWARE, YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD, AND AGREE TO BE BOUND BY THE TERMS OF THIS COPYRIGHT AGREEMENT. IF YOU DO NOT AGREE TO THESE TERMS, YOU MUST CEASE ALL USE OF THE SOFTWARE IMMEDIATELY AND PERMANENTLY DELETE ANY COPIES IN YOUR POSSESSION OR CONTROL.

